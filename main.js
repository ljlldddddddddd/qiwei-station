import App from './App'

// #ifndef VUE3
import Vue from 'vue'
Vue.config.productionTip = false
App.mpType = 'app'
import uView from '@/uni_modules/uview-ui'
Vue.use(uView)

import VueI18n from "vue-i18n";
Vue.use(VueI18n);
const i18n = new VueI18n({
    locale: !uni.getStorageSync('lang') ?
        "cn" : uni.getStorageSync('lang'),
    messages: {
        cn: require("./lang/cn.json"), 
		en: require("./lang/en.json"), 
    }
});
Vue.prototype._i18n = i18n

 //引入全局请求插件
import { http,api } from '@/config/common.js' // 全局挂载引入，配置相关在该index.js文件里修改
Vue.prototype.$http = http
Vue.prototype.$api = api
 
import store from './store'
Vue.prototype.$store = store;

import share from '@/config/share.js'
Vue.mixin(share)


const app = new Vue({
	i18n,
    ...App
})
app.$mount()
// #endif

// #ifdef VUE3
import { createSSRApp } from 'vue'
export function createApp() {
  const app = createSSRApp(App)
  return {
    app
  }
}
// #endif
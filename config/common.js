import Request from '@/js_sdk/luch-request/luch-request/index.js'
const apiBaseUrl = 'http://39.106.111.151:82/'
const BaseImgUrl = "http://39.106.111.151:81/"
const api = new Request()
const http = new Request()
export {
	http,
	api,
	apiBaseUrl, 
	BaseImgUrl,
}

api.setConfig((config) => { 
	/* 设置全局配置 */
	config.baseURL = apiBaseUrl 
	config.header = {
		...config.header, 
	} 
	return config
})

api.interceptors.request.use((config) => { // 可使用async await 做异步操作
	config.baseURL = apiBaseUrl 
	config.header = {
		...config.header,
	}   
	config.data.lang =  uni.getStorageSync('lang') ? uni.getStorageSync('lang') : 'cn'
	config.data.token=  uni.getStorageSync('userInfo').token
	// if (config.data.zpy) {
	// 	config.baseURL = apiBaseUrlV2 
	// } else {
	// 	const url = config.url;
	// 	const urlPath = url.split('?')[1].split('&')[0];
	// 	if (urlPath != 'm=UserLogin') {
	// 		const Bid = uni.getStorageSync('Bid');
	// 		if (!Bid) {
	// 			uni.redirectTo({
	// 				url: '/pages/login/login'
	// 			})
	// 		}
	// 	}
	// }
	
	
	// if (token) { 
	// 	config.header['token'] = token
	// }


	/**
	 /* 演示
	 if (!token) { // 如果token不存在，return Promise.reject(config) 会取消本次请求
	    return Promise.reject(config)
	  }
	 **/
	return config
}, config => { // 可使用async await 做异步操作
	return Promise.reject(config)
})


// 请求后
api.interceptors.response.use((response) => {
	let resCode = response.data.code
	if (resCode == '204' || resCode == '203') {
		uni.navigateTo({
			url:'/pages/login/login'
		})
	} 
	return response
}, (response) => { 
	/*  对响应错误做点什么 （statusCode !== 200）*/
	// console.log(response)
	// console.log(response.statusCode);

	// if (response.statusCode == 401 || response.statusCode == 402) {
	// 	uni.clearStorageSync();
	// 	uni.showToast({
	// 		title: "请先登录",
	// 		icon: 'none'
	// 	})

	// 	setTimeout(function() {
	// 		uni.navigateTo({
	// 			url: "/pages/common/login"
	// 		})
	// 	}, 1000);
	// }

	// if (response.statusCode == 403) {
	// 	uni.clearStorageSync();
	// 	uni.showToast({
	// 		title: "请先登录",
	// 		icon: 'none'
	// 	})

	// 	setTimeout(function() {
	// 		// uni.navigateTo({
	// 		// 	url: "/pages/common/login"
	// 		// })
	// 	}, 1000);
	// }

	return Promise.reject(response)
})

